import express from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
import Knex from 'knex';
import cors from 'cors';
import dotenv from 'dotenv';
import multer from 'multer';
import { PyController } from './controllers/PyController';
import { UserService } from './services/UserService';
import { AuthController } from './controllers/AuthController';
import { UserController } from './controllers/UserController';
import { createIsLoggedIn } from './guard';
import { Bearer } from 'permit';



dotenv.config();


const knexConfig = require('./knexfile')
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development'])

declare global{
    namespace Express{
        interface Request{
            user?: {
              id: number;
              username: string;
            }
        }
    }
  }

export const storage = multer.diskStorage({
    destination: function (req, file ,cb) {
        cb (null, `${__dirname}/uploads`);
    },
    filename: function (req, file , cb) {
        cb (null, file.originalname);
    }
})

export const uploadLocal = multer ({storage});


const app = express();

app.use(expressSession({
    secret: 'SoundBilibala',
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json());

app.use(cors({
    origin: [
        process.env.REACT_DOMAIN!
    ]
}))

const userService = new UserService(knex)
const authController = new AuthController(userService)
const userController = new UserController(userService)
const pyController = new PyController(knex);

const permit = new Bearer({
  query:"access_token"
})

const isLoggedIn = createIsLoggedIn(permit, userService)

app.post('/login', authController.login);
app.post('/loginFacebook', authController.loginFacebook);
app.get('/user', isLoggedIn(['user', 'admin']), userController.getCurrentUser);
app.post('/uploadAudio',isLoggedIn(['user','admin']), uploadLocal.single('audio'), pyController.postFile);
app.get('/checkFiles', isLoggedIn(['user','admin']), pyController.getFile);
app.post('/register',userController.creatUser);


const PORT = process.env.PORT || 8000
app.listen(PORT, () => {
    console.log('Listening at port ' + PORT)
})
