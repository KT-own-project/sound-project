import * as Knex from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    await knex("uploadfiles").del()
    await knex("users").del()

    await knex("users").insert([
        { username: "admin", password: await hashPassword('admin') },
    ]).into("users")

};
