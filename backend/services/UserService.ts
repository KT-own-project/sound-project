import Knex from "knex";

export class UserService {
    public constructor(private knex: Knex) {

    }

    public async getUserByUsername(username: string) {
        return (await this.knex.raw(/*sql*/`SELECT * FROM users WHERE username = ?`, [username])).rows[0];
    }

    public async getUserById(id: number) {
        return (await this.knex.raw(/*sql*/`SELECT * FROM users WHERE id = ?`, [id])).rows[0];
    }

    public async creatUser(username: string, password: string) {
        return (await this.knex.raw(/*sql*/`INSERT INTO users (username,password) VALUES (?, ?)`, [username, password]))
    }

    public async getUserByFacebookId (id: number) {
        return (await this.knex.raw(/*sql*/`SELECT * FROM users WHERE facebook_id = ?`, [id])).rows[0];
    }

    public async createFacebookUser(username: string, facebookId: string){
        return(await this.knex.raw(/*sql*/ `INSERT INTO users (username, facebook_id) VALUES (?,?) RETURNING id`,[
            username, facebookId
        ])).rows[0];
    }
    
}