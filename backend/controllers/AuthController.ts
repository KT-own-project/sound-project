import { UserService } from "../services/UserService";
import express from 'express'
import jwtSimple from 'jwt-simple'
import { checkPassword } from "../hash";
import jwt from "../jwt";
import fetch from 'node-fetch';

export class AuthController {
    public constructor(private userService: UserService) {

    }

    public login = async (req: express.Request, res: express.Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                return res.status(400).json({error: 'No username or password'})
            }

            const user = await this.userService.getUserByUsername(req.body.username);
            if (user == null) {
                return res.status(401).json({error: 'Incorrect username or password'})
            }
            if (!await checkPassword(req.body.password, user.password)) {
                return res.status(401).json({error: 'Incorrect username or password'})
            }

            const payload = {
                id: user.id
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            return res.json({
                token: token,
                user: user
            });
        } catch (e) {
            console.error(e);
            return res.status(500).json({error: 'Unknown error'});
        }
    }
    public loginFacebook = async (req: express.Request, res: express.Response) =>{
        try {
            if(!req.body.accessToken){
                return res.status(400).json({error: 'No token'})
            }

            const facebookRes = await fetch (`https://graph.facebook.com/me?access_token=${req.body.accessToken}&fields=id,name,email,picture`)
            const json = await facebookRes.json();

            if(!json.id){
                return res.status(400).json({error: 'No token'})
            }

            let user = await this.userService.getUserByFacebookId(json.id);
            if(!user){
                user = await this.userService.getUserByFacebookId(json.email);

                if (!user){
                    user = await this.userService.createFacebookUser(json.email, json.id)
                    user = await this.userService.getUserById(user.id)
                }
            }
            const payload = {
                id: user.id
            };

            const token = jwtSimple.encode(payload, jwt.jwtSecret)
            return res.json({
                token: token,
                user: user
            });

        }catch(e){
            console.error(e);
            return res.status(500).json({error: 'Unknown error'});
        }
    }
}