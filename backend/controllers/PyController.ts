import express from 'express';
import fs from 'fs';
import fetch from 'node-fetch'
import FormData from "form-data"
import Knex from 'knex';
import dotenv from 'dotenv';
dotenv.config();

export class PyController {
    constructor(private knex: Knex) {

    }

    public postFile = async (req: express.Request, res: express.Response) => {
        const id = (await this.knex.raw(/*sql*/`INSERT INTO uploadfiles (filename, completed, user_id) VALUES (?, ?, ?)  RETURNING id`, [
            req.file.filename, req.body.completed, req.user?.id
        ])).rows[0].id;
        res.json({message: "upload success"});
        const numbers = req.body.numbers;
        const formData = new FormData();
        const stream = fs.createReadStream(req.file.path)
        formData.append('the_file', stream, req.file.filename)
        formData.append('numbers',numbers);
        const fetchRes = await fetch(`${process.env.PYTHON_SERVER}/speech`, {
            method: 'POST',
            body: formData
        })
        const text = await fetchRes.text();
        if (text != null) {
            await this.knex.raw(/*sql*/`UPDATE uploadfiles SET completed = true WHERE ID = ?`, [id])
            await this.knex.raw(/*sql*/`UPDATE uploadfiles SET result = ? WHERE ID = ?`, [text,id])
        }
        fs.unlinkSync(req.file.path)
    }

    public getFile = async (req: express.Request, res: express.Response) => {
        const { rows } = await this.knex.raw(/*sql*/`SELECT * FROM uploadfiles WHERE user_id = ?`, [req.user?.id!])
        res.json(rows);
    }
}