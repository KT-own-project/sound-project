import { UserService } from "../services/UserService";
import express from 'express'
import { hashPassword } from "../hash";

export class UserController {
    
    public constructor(private userService: UserService) {

    }

    public getCurrentUser = (req: express.Request, res: express.Response) => {
        res.json(req.user);
    }

    public creatUser = async(req: express.Request, res: express.Response) => {
        try{
            const {username, password} = req.body;
            await this.userService.creatUser(username, await hashPassword(password));
            res.json({success:true})
        } catch (err) {
            res.status(500).json({message: "internal server error"})
        }
    }
}