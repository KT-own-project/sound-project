import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('uploadfiles', (table)=>{
        table.increments();
        table.string('filename');
        table.boolean('completed');
        table.integer('user_id').unsigned();
        table.foreign('user_id').references('users.id');
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('uploadfiles');
}

