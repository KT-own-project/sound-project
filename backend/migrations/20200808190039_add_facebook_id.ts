import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table=>{
        table.string('facebook_id').unique();
        table.string('google_id').unique();
    });
};


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('users', table=>{
        table.dropColumn('facebook_id');
        table.dropColumn('google_id');
    });
};

