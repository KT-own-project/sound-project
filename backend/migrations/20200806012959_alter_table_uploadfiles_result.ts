import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('uploadfiles', table=>{
        table.text('result');
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('uploadfiles', table=>{
        table.dropColumn('result');
    });
}

