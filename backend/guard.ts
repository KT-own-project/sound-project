import express from 'express'
import { UserService } from './services/UserService'
import { Bearer } from 'permit'
import jwtSimple from 'jwt-simple'
import jwt from './jwt'

export const createIsLoggedIn = (permit: Bearer,  userService: UserService) => {
    return (roles: string[], optional?: boolean) => {
        return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
            try {
                const token = permit.check(req);
        
                if (!token) {
                    return optional ? next() : res.status(401).json({error: 'Unauthorized'})
                }
        
                const payload = jwtSimple.decode(token, jwt.jwtSecret);
                const user = await userService.getUserById(payload.id)
                if (!user) {
                    return optional ? next() : res.status(401).json({error: 'Unauthorized'})
                }

                // if (roles.indexOf(user.role) == -1) {
                //     return optional ? next() : res.status(403).json({error: 'Permission Denied'})
                // }
    
                const { password, ... userWithoutPassword } = user;
                req.user = userWithoutPassword;
    
                return next();
            } catch (e) {
                console.error(e);
                return optional ? next() : res.status(401).json({error: 'Unauthorized'})
            }
        }
    }
}