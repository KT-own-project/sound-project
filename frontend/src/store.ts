
import { combineReducers, createStore, compose, applyMiddleware } from "redux";
import { connectRouter, routerMiddleware, RouterState, RouterAction } from "connected-react-router"
import { createBrowserHistory } from "history";
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { UserActions } from "./redux/user/actions";
import { UserState, userReducer } from "./redux/user/reducer";
import { AuthState, authReducer } from "./redux/auth/reducer";
import { AuthActions } from "./redux/auth/actions";
import { AudioFileState, fileReducer } from "./redux/uploadFile/reducer";
import { AudioFileActions } from "./redux/uploadFile/action";
import { LoadingState, loadingReducer } from "./redux/loading/reducer";
import { LoadingActions } from "./redux/loading/actions";


export const history = createBrowserHistory();

export interface RootState {
    audioFiles: AudioFileState,
    auth: AuthState,
    user: UserState,
    router: RouterState,
    loading: LoadingState
}

export type RootActions = RouterAction | UserActions | AuthActions | LoadingActions | AudioFileActions;

const reducers = combineReducers<RootState>({
    audioFiles: fileReducer,
    auth: authReducer,
    loading: loadingReducer,
    user: userReducer,
    router: connectRouter(history)
})

declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore<RootState, RootActions, {}, {}>(reducers, composeEnhancers(
    applyMiddleware(thunk),
    applyMiddleware(routerMiddleware(history))
))
