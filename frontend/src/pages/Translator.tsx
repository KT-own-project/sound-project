import React, { useState, useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'reactstrap';
import { useFormState } from 'react-use-form-state';
import { RootState } from '../store';
import { fetchAudioFiles } from '../redux/uploadFile/thunk';
import FileStatus from './FileStatus';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle, faUpload } from '@fortawesome/free-solid-svg-icons'

export function Translator() {
    const dispatch = useDispatch();
    const fileInputRef = useRef<any>();
    const [selectedFile, setSelectedFile] = useState<File[]>([]);
    const [validFiles, setValidFiles] = useState<File[]>([]);
    const token = useSelector((state: RootState) => state.auth.token)
    const [formState, { select }] = useFormState();
    const iconUpload = <FontAwesomeIcon className="uploadIcon" color="black" size="lg" icon={faUpload} />
    const iconClose = <FontAwesomeIcon className="closeIcon" color="black" size="lg" icon={faTimesCircle} />


    useEffect(() => {
        let filteredArray = selectedFile.reduce((file: File[], current) => {
            const x = file.find(item => item.name === current.name);
            if (!x) {
                return file.concat([current]);
            } else {
                return file;
            }
        }, []);
        setValidFiles([...filteredArray]);
        dispatch(fetchAudioFiles())
    }, [selectedFile, dispatch]);

    const dragOver = (e: Event | React.DragEvent) => {
        e.preventDefault();
    }

    const dragEnter = (e: Event | React.DragEvent) => {
        e.preventDefault();
    }

    const dragLeave = (e: Event | React.DragEvent) => {
        e.preventDefault();
    }

    const fileDrop = (e: React.DragEvent) => {
        e.preventDefault();
        const files = e.dataTransfer.files;
        if (files.length) {
            handleFiles(files);
        }
    }

    const fileInputClicked = () => {
        fileInputRef.current.click();
    }

    const filesSelected = () => {
        if (fileInputRef.current.files.length) {
            handleFiles(fileInputRef.current.files);
        }
    }

    const handleFiles = (files: any) => {
        for (let i = 0; i < files.length; i++) {
            if (validateFile(files[i])) {
                setSelectedFile(prevArray => [...prevArray, files[i]]);
                console.log(selectedFile)
            } else {
                files[i]['invalid'] = true;
            }
        }
    }


    const fileSize = (size: number) => {
        if (size === 0) return '0 Bytes';
        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        const i = Math.floor(Math.log(size) / Math.log(k));
        return parseFloat((size / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
    }

    const fileType = (fileName: any) => {
        return fileName.slice(fileName.lastIndexOf('.') + 1, fileName.length) || fileName;
    }

    const validateFile = (file: File) => {
        const validTypes = 'audio'
        if (file.type.indexOf(validTypes) === -1) {
            return false;
        }
        return true;
    }

    const removeFile = (name: string) => {
        // find the index of the item
        // remove the item from array

        const validFileIndex = validFiles.findIndex(e => e.name === name);
        validFiles.splice(validFileIndex, 1);
        // update validFiles array
        setValidFiles([...validFiles]);
        const selectedFileIndex = selectedFile.findIndex(e => e.name === name);
        selectedFile.splice(selectedFileIndex, 1);
        // update selectedFiles array
        setSelectedFile([...selectedFile]);
    }

    return (
        <div className="container">
        <div className="content-drop">
            <div className="container-drop">
                <div className="drop-container"
                    onDragOver={dragOver}
                    onDragEnter={dragEnter}
                    onDragLeave={dragLeave}
                    onDrop={fileDrop}
                    onClick={fileInputClicked}
                >
                    <div className="drop-message">
                        <div className="upload-icon"></div>
                        <div>{iconUpload}</div>
                        Drag & Drop file here or click to upload
                        </div>
                    <input
                        ref={fileInputRef}
                        className="file-input"
                        type="file"
                        multiple
                        onChange={filesSelected}
                    />
                </div>
                <div className="file-display-container">
                    {
                        validFiles.map((data, i) =>
                            <div className="file-status-bar" key={i}>
                                <div>
                                    <div className="file-type-logo"></div>
                                    <div className="file-type">{fileType(data.name)}</div>
                                    <span className={`file-name`}>{data.name}</span>
                                    <span className="file-size">({fileSize(data.size)})</span>
                                </div>
                                <form className="upload-bar">
                                    <div className="selector-bar">Speakers: <select {...select('numbers')}>
                                        <option>2</option>
                                        <option>over 2</option>
                                    </select></div>
                                    <Button color="warning" onClick={async event => {
                                        event.preventDefault();
                                        const formData = new FormData();
                                        formData.append('audio', data);
                                        formData.append('completed', '0');
                                        formData.append('numbers',formState.values.numbers);
                                        console.log(formData);
                                        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/uploadAudio`, {
                                            method: 'POST',
                                            headers: {
                                                // 'Content-Type': 'application/json',
                                                'Authorization': `Bearer ${token}`
                                            },
                                            body: formData
                                        })
                                        const json = await res.json();
                                        removeFile(data.name);
                                        console.log(json);
                                    }}>Upload Files</Button>
                                    <div className="file-remove" onClick={() => removeFile(data.name)}>{iconClose}</div>
                                </form>

                            </div>
                        )
                    }
                    <h6> </h6>
                    <FileStatus />
                </div>

            </div>
        </div>
    </div>
    )
};