import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FileBar from './FileBar';
import { RootState } from '../store';
import { fetchAudioFiles } from '../redux/uploadFile/thunk';

const FileStatus = () => {
    const dispatch = useDispatch();
    const audioFilesIds = useSelector((state: RootState) => state.audioFiles.audioFileIds);
    const audioFiles = useSelector((state: RootState) => audioFilesIds.map(id => state.audioFiles.audioFileById[id]))
    
    useEffect(() => {
        dispatch(fetchAudioFiles())
    }, [dispatch])

    return (
        <div>
            <h4>Transcription Status</h4>
            {
                audioFiles.map((audioFile, i) => (
                    <div  key={i}>
                        
                        <FileBar {...audioFile}/>
                    </div>
                ))
            }
        </div>
    )
}

export default FileStatus;