import React, { useState } from 'react';
import { Collapse, Button, Card, CardBody } from 'reactstrap';
import { AudioFile } from '../redux/uploadFile/reducer';

const FileBar = (audioFile: AudioFile) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div>
            <div className="uploadedFile-bar">
                <div>
                    <span>{audioFile.created_at.slice(0, 10)}</span>
                    <span className="audio-name">{audioFile.filename}</span>
                </div>
                <span>{audioFile.completed ? <Button className="resultBtn" color="btn btn-secondary" onClick={() => setIsOpen(!isOpen)} style={{ marginBottom: '1rem' }}>Result</Button> : "Processing"}</span>
            </div>

            <div className="translationResult">
                <Collapse isOpen={isOpen}>
                    <Card>
                        <CardBody>
                            <textarea className="resultTextarea" defaultValue={audioFile.result} />
                        </CardBody>
                    </Card>
                </Collapse>
            </div>
        </div>
    )
}

export default FileBar;