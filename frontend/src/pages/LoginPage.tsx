import React, { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { useFormState } from 'react-use-form-state';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../store';
import { clearError, inputError } from '../redux/auth/actions';
import { login, register } from '../redux/auth/thunk';
import desktopImage from '../images/desktopImage.jpg';
import mobileImage from '../images/mobileImage2.jpg';
import soundImage from '../images/sound-wave.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUnlockAlt, faUser, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { CSSTransition } from 'react-transition-group';


export function LoginPage() {
    const [formState, { text, password }] = useFormState();
    const [isLogin, setIsLogin] = useState(true);
    const [isRegister, setIsRegister] = useState(false);
    const dispatch = useDispatch();
    const errorMessage = useSelector((state: RootState) => state.auth.error);
    const imageUrl = window.innerWidth >= 650 ? soundImage : mobileImage;
    const iconUser = <FontAwesomeIcon className="iconUser" color="black" size="lg" icon={faUser} />
    const iconLock = <FontAwesomeIcon className="iconLock" color="black" size="lg" icon={faUnlockAlt} />
    const iconBackward = <FontAwesomeIcon className="iconUser" color="black" size="lg" icon={faArrowLeft} />


    useEffect(() => {
        if (errorMessage !== null) {
            const timer = setTimeout(() => {
                dispatch(clearError())
            }, 3000);
            return () => {
                clearTimeout(timer);
            };
        };
    }, [errorMessage, dispatch]);


    return (
        <div className={isLogin ? 'formBody' : 'formBody2'}>
            <div className="backgroundPic" style={{ backgroundImage: `url(${imageUrl})` }}></div>
            <div>
                <Container className="loginBox">
                    {isLogin && (
                        <><div className="toRegister" onClick={() => setIsRegister(true)}>Register?</div>
                            <Form className="loginForm" onSubmit={event => {
                                event.preventDefault();
                                dispatch(login(formState.values.username, formState.values.password));
                            }}>
                                <Form.Group className="formTitle">
                                    <Form.Label>Login</Form.Label>
                                </Form.Group>

                                <Form.Group className="errorMessage">
                                    <Form.Label>{errorMessage}</Form.Label>
                                </Form.Group>

                                <Form.Group className="loginUsername">
                                    {iconUser}
                                    <Form.Control className="formPlaceholder" placeholder="Enter Username" {...text('username')}></Form.Control>
                                </Form.Group>

                                <Form.Group className="loginPassword">
                                    {iconLock}
                                    <Form.Control className="formPlaceholder" placeholder="Enter Password" {...password('password')}></Form.Control>
                                </Form.Group>

                                <Form.Group className="submitBtn">
                                    <Button className="formBtn" variant="primary" type="submit">Submit</Button>
                                </Form.Group>
                            </Form></>
                    )}
                </Container>

                <CSSTransition
                    in={isRegister}
                    timeout={300}
                    classNames="registerBox"
                    unmountOnExit
                    onEnter={() => setIsLogin(false)}
                    onExited={() => setIsLogin(true)}
                >
                    <Container>
                        <div className="backToLogin" onClick={() => setIsRegister(false)}>
                            {iconBackward} Login
                    </div>
                        <Form className="RegisterForm" onSubmit={event => {
                            event.preventDefault();
                            if (formState.values.password !== formState.values.passwordConfirm) {
                                dispatch(inputError("password doesn't match"))
                            } else {
                                dispatch(register(formState.values.username, formState.values.password));
                                setIsRegister(false);
                                dispatch(inputError("Registration success"))
                            }
                        }}>

                            <Form.Group className="formTitle">
                                <Form.Label>Register</Form.Label>
                            </Form.Group>

                            <Form.Group className="errorMessage">
                                <Form.Label>{errorMessage}</Form.Label>
                            </Form.Group>

                            <Form.Group className="register">
                                {iconUser}
                                <Form.Control className="formPlaceholder" placeholder="Enter Username"{...text('username')} required minLength={2}></Form.Control>
                            </Form.Group>

                            <Form.Group className="register">
                                {iconLock}
                                <Form.Control className="formPlaceholder" placeholder="Enter Password"{...password('password')} required minLength={6}></Form.Control>
                            </Form.Group>
                            <Form.Group className="register">
                                {iconLock}
                                <Form.Control className="formPlaceholder" placeholder="Enter Password again"{...password('passwordConfirm')} required></Form.Control>
                            </Form.Group>

                            <Form.Group className="submitBtn">
                                <Button className="formBtn" variant="primary" type="submit">Submit</Button>
                            </Form.Group>
                        </Form>
                    </Container>
                </CSSTransition>


            </div>
        </div>
    )
}