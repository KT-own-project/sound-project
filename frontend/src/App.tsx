import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { restoreLogin } from './redux/auth/thunk';
import { AppRoute } from './Route';


function App() {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(restoreLogin())
  }, [dispatch]);

  return (
    <div className="App">
      <AppRoute />
    </div>
  );
};

export default App;

