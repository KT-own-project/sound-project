import { LoadingActions } from "./actions";

export interface LoadingState {
    isLoading: number;
};

const initialState = {
    isLoading: 0
};

export function loadingReducer(state: LoadingState = initialState, action: LoadingActions): LoadingState {
    switch (action.type) {
        case '@@loading/FINISH_LOADING':
            return {
                isLoading: state.isLoading - 1
            };
        case '@@loading/LOADING':
            return {
                isLoading: state.isLoading + 1
            };
        default: 
            return state;
    };
};