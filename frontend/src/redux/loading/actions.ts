export function loading() {
    return {
        type: '@@loading/LOADING' as '@@loading/LOADING'
    };
};

export function finishLoading() {
    return {
        type: '@@loading/FINISH_LOADING' as '@@loading/FINISH_LOADING'
    };
};

export type LoadingActions = ReturnType<typeof loading | typeof finishLoading>;