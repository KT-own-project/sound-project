import { push } from "connected-react-router";
import { RootState, ThunkDispatch } from "../../store";
import { finishLoading, loading } from "../loading/actions";
import { loginFailed, loginSuccess, logoutSuccess } from "./actions";

export function logout() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        localStorage.removeItem('token')
        dispatch(logoutSuccess())
    };
}

export function login(username: string, password: string){
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        dispatch(loading());
        try{
             const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/login`, {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    username, password
                })
            });
            
            const json = await res.json();
            if(res.status !== 200){
                return dispatch(loginFailed(json.error));
            };

            if(!json.token) {
                return dispatch(loginFailed('Network error'));
            };

            const returnPath = (getState().router.location.state as any)?.from

            localStorage.setItem('token', json.token);
            dispatch(loginSuccess(json.token, json.user));
            dispatch(push(returnPath || '/'));

        }catch (e){
            console.error(e);
            return dispatch(loginFailed('Unknown error'));
        }finally{
            dispatch(finishLoading());
        };
    };
};

export function loginFacebook(accessToken: string){
    return async (dispatch: ThunkDispatch, getState: () => RootState) =>{
        dispatch(loading());
        try{
             const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/loginFacebook`, {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    accessToken
                })
            });
            
            const json = await res.json();
            if(res.status !== 200){
                return dispatch(loginFailed(json.error));
            };

            if(!json.token) {
                return dispatch(loginFailed('Network error'));
            };

            const returnPath = (getState().router.location.state as any)?.from

            localStorage.setItem('token', json.token);
            dispatch(loginSuccess(json.token, json.user));
            dispatch(push(returnPath || '/'));

        }catch (e){
            console.error(e);
            return dispatch(loginFailed('Unknown error'));
        }finally{
            dispatch(finishLoading());
        };
    }
}

export function restoreLogin() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        try {
            const token = localStorage.getItem('token');
            if (token == null) {
                dispatch(logout());
                return;
            };
            const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/user`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            const json = await res.json();

            if (json.id) {
                dispatch(loginSuccess(token, json))
            } else {
                dispatch(logout());
            };
        } catch (e) {
            console.error(e);
            dispatch(logout());
        };
    };
};

export function register(username: string, password: string){
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        dispatch(loading());
        try{
             const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/register`, {
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    username, password
                })
            });
            
            const json = await res.json();
            console.log(json);

        }catch (e){
            console.error(e);
            return dispatch(loginFailed('Unknown error'));
        }finally{
            dispatch(finishLoading());
        };
    };
};