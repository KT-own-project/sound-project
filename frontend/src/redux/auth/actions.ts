export interface User {
    id: number;
    username: string;
    role: string;
};

export function loginSuccess(token: string, user: User){
    return {
        type: '@@auth/LOGIN_SUCCESS' as '@@auth/LOGIN_SUCCESS',
        token,
        user
    };
};

export function loginFailed(error: string){
    return {
        type: '@@auth/LOGIN_FAILED' as '@@auth/LOGIN_FAILED',
        error
    };
};

export function logoutSuccess(){
    return {
        type: '@@auth/LOGOUT' as '@@auth/LOGOUT'
    };
};

export function clearError(){
    return {
        type: '@@auth/CLEAR_ERROR' as '@@auth/CLEAR_ERROR'
    };
};

export function inputError(error: string){
    return {
        type: '@@auth/INPUT_ERROR' as '@@auth/INPUT_ERROR',
        error
    };
};

export type AuthActions = ReturnType <typeof loginSuccess | typeof loginFailed | typeof logoutSuccess | typeof clearError | typeof inputError>;