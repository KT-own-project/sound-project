import { RootState, ThunkDispatch } from "../../store";
import { loadFiles } from "./action";

export function fetchAudioFiles() {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/checkFiles`, {
            headers: {
                'Authorization': `Bearer ${getState().auth.token}`
            }
        });
        const json = await res.json();

        dispatch(loadFiles(json))
    }
}