import { AudioFileActions } from "./action";

export interface AudioFile {
    id: number;
    filename: string;
    completed: boolean;
    user_id: number;
    result: string;
    created_at: string;
    updated_at: string;
}

export interface AudioFileState {
    audioFileIds: number[],
    audioFileById: {
        [id: string]: AudioFile
    }
}

export const initialState: AudioFileState = {
    audioFileIds: [],
    audioFileById: {}
}

export function fileReducer(state: AudioFileState = initialState, action: AudioFileActions):AudioFileState {
    switch (action.type) {
        case '@@AUDIOFILES/LOADED_FILES':
            const newFileById: {
                [id: string]: AudioFile
            } = {}
            for (const audioFile of action.AudioFiles) {
                newFileById[audioFile.id] = audioFile
            }
            return{
                audioFileIds: action.AudioFiles.map(audioFile => audioFile.id),
                audioFileById: newFileById
            }
    }
    
    return state;
}