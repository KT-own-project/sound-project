import { AudioFile } from "./reducer";

export function loadFiles(AudioFiles: AudioFile[]) {
    return {
        type: "@@AUDIOFILES/LOADED_FILES" as "@@AUDIOFILES/LOADED_FILES",
        AudioFiles: AudioFiles
    };
}

export type AudioFileActions = ReturnType<typeof loadFiles>;

