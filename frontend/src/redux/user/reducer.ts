import { User, UserActions } from "./actions";


export interface UserState {
    userIds: number[];
    userById: {
        [id: string]: User
    };
};

const initialState: UserState = {
    userIds: [],
    userById:{}
};

export function userReducer(state: UserState = initialState, action: UserActions): UserState {
    switch(action.type) {
        case '@@user/LOADED_USERS':
            const newUserById: {
                [id: string]: User
            } = {};
            for(const user of action.users){
                newUserById[user.id] = user;
            };
            return {
                userIds: action.users.map(user => user.id),
                userById: newUserById
            };
        default:
            return state;
    };
};