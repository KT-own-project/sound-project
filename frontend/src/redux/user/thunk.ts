import { ThunkDispatch } from "../../store";
import { loadedUsers } from "./actions";

export function fetchUsers() {
    return async (dispatch: ThunkDispatch) =>{
        const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/users`);
        const json = await res.json();
        
        dispatch(loadedUsers(json));
    };
};