import { ThunkDispatch } from "../../store";

export interface User {
    id:         number;
    username:   string;
    password:   string;
    created_at: string;
    updated_at: string;
};


export function loadedUsers (users: User[]) {
    return {
        type: '@@user/LOADED_USERS' as '@@user/LOADED_USERS',
        users
    };
};

export type UserActions = ReturnType <typeof loadedUsers>;