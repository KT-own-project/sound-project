import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { RootState } from './store';
import { logout, restoreLogin } from './redux/auth/thunk';
import { LoginPage } from './pages/LoginPage';
import { PrivateRoute } from './PrivateRoute';
import { Translator } from './pages/Translator';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'



export function AppRoute() {
  const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
  const dispatch = useDispatch();
  const iconLogout = <FontAwesomeIcon className="iconLogout" color="black" size="lg" icon={faSignOutAlt} />

  useEffect(() => {
    dispatch(restoreLogin())
  }, [dispatch]);

  return (
    <div>
      {isAuthenticated ?
        <Container>
          <Navbar className="NavBar" bg="white" expand="sm">
            <Navbar.Brand href="/"></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              <Nav className="mr-auto">
              </Nav>
              <Form inline>
                <div className="inlineForm">
                  <div className="LogoutControl">
                    {isAuthenticated ? <Nav.Link onClick={() => dispatch(logout())} href="/">{iconLogout}Logout</Nav.Link> : <Nav.Link href="/login">Login</Nav.Link>}
                  </div>
                </div>
              </Form>
            </Navbar.Collapse>
          </Navbar>
        </Container>
        : null
      }
      { isAuthenticated === null ?
        <div className="loading"></div>
        :
        <Switch>
          <PrivateRoute path="/" exact><Translator /></PrivateRoute>
          <Route path="/login"><LoginPage /> </Route>
        </Switch>
      }
    </div>
  )
};
