import React from 'react';
import { RouteProps, Route, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from './store';
import { loginFailed } from './redux/auth/actions';

export function PrivateRoute(props: RouteProps & {roles?: string[]}){
    const isAuthenticated = useSelector((state: RootState) => state.auth.isAuthenticated);
    const role = useSelector((state: RootState) => state.auth.user?.role || '');
    const dispatch = useDispatch();

    const render = () => {
        return <Redirect to={{
            pathname: '/login',
            state: {from: props.path}
        }} />
    };

    if(isAuthenticated){
        if(props.roles && props.roles.indexOf(role) === -1 ) {
            dispatch(loginFailed('Permission denied'));
            const { children, component, ...remainingProps } = props;
            return <Route {...remainingProps} render = {render} />
        }else{
            return <Route {... props} />;            
        };
    }else{
        const { children, component, ...remainingProps } = props;
        return <Route {...remainingProps} render = {render} />
    };
};