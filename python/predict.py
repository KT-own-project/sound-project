# %%
import io
import os
import librosa
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "GoogleApi/google.json"
client = speech.SpeechClient()

# %%
layer_name = 'global_max_pooling2d'
model = tf.keras.models.load_model('models/resnet.h5', compile=False)
intermediate_layer_model = Model(
    inputs=model.input, outputs=model.get_layer(layer_name).output)

# 讀取音頻數據


def load_data(data_path):
    wav, sr = librosa.load(data_path, sr=16000)
    intervals = librosa.effects.split(wav, top_db=20)
    wav_output = [8]
    for sliced in intervals:
        wav_output.extend(wav[sliced[0]:sliced[1]])
    assert len(wav_output) >= 4000, "有效音頻小於0.5s"
    wav_output = np.array(wav_output)
    ps = librosa.feature.melspectrogram(
        y=wav_output, sr=sr, hop_length=256).astype(np.float32)
    ps = ps[np.newaxis, ..., np.newaxis]
    return ps


def infer(audio_path):
    data = load_data(audio_path)
    feature = intermediate_layer_model.predict(data)
    return feature


def predict(nums, folderName):
    speaker1 = set()
    speaker2 = set()
    for i in range(nums-1):
        speaker1.add('test0.flac')
        # 要預測的兩個人的音頻文件
        person1 = 'segment/%s/' % folderName + 'test%s.flac' % i
        person2 = 'segment/%s/' % folderName + 'test%s.flac' % (i+1)
        feature1 = infer(person1)[0]
        feature2 = infer(person2)[0]
        # 對角餘弦值
        dist = np.dot(feature1, feature2) / \
            (np.linalg.norm(feature1) * np.linalg.norm(feature2))
        if dist > 0.75:
            if 'test%s.flac' % i in speaker1:
                speaker1.add("test%s.flac" % (i+1))
            else:
                speaker2.add("test%s.flac" % (i+1))
        else:
            if 'test%s.flac' % i in speaker1:
                speaker2.add("test%s.flac" % (i+1))
            else:
                speaker1.add("test%s.flac" % (i+1))
    print(speaker1)
    print(speaker2)
    resultTxt = ""
    for j in range(nums):
        if 'test%s.flac' % j in speaker1:
            result = 'Speaker 1: {}'.format(
                googleSpeech('segment/%s/' % folderName + 'test%s.flac' % j))
            # with open("txt/%s.txt"%folderName, mode='a') as file:
            #     file.write(result)
            #     file.write('\n')
            resultTxt += result
            resultTxt += '\n'
        else:
            result = 'Speaker 2: {}'.format(
                googleSpeech('segment/%s/' % folderName + 'test%s.flac' % j))
            # with open("txt/%s.txt" % folderName, mode='a') as file:
            #     file.write(result)
            #     file.write('\n')
            resultTxt += result
            resultTxt += '\n'
    print('Done')
    print(resultTxt)
    return resultTxt


def googleSpeech(fileName):
    file_name = fileName

    with io.open(file_name, 'rb') as audio_file:
        content = audio_file.read()
        audio = types.module.RecognitionAudio(content=content)

    config = types.module.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.FLAC,
        sample_rate_hertz=16000,
        language_code='en-US'
    )

    response = client.recognize(config, audio)

    for result in response.results:
        return(result.alternatives[0].transcript)
