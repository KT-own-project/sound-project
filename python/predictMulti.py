# %%
import io
import os
import librosa
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types


os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "GoogleApi/google.json"
client = speech.SpeechClient()

# %%
layer_name = 'global_max_pooling2d'
model = tf.keras.models.load_model('models/resnet.h5', compile=False)
intermediate_layer_model = Model(
    inputs=model.input, outputs=model.get_layer(layer_name).output)

# 讀取音頻數據


def load_data(data_path):
    wav, sr = librosa.load(data_path, sr=16000)
    intervals = librosa.effects.split(wav, top_db=20)
    wav_output = [8]
    for sliced in intervals:
        wav_output.extend(wav[sliced[0]:sliced[1]])
    assert len(wav_output) >= 4000, "有效音頻小於0.5s"
    wav_output = np.array(wav_output)
    ps = librosa.feature.melspectrogram(
        y=wav_output, sr=sr, hop_length=256).astype(np.float32)
    ps = ps[np.newaxis, ..., np.newaxis]
    return ps


def infer(audio_path):
    data = load_data(audio_path)
    feature = intermediate_layer_model.predict(data)
    return feature


def innerPredict(file1, personFile, folderName):
    # 要預測的兩個人的音頻文件
    person1 = 'segment/%s/' % folderName + file1
    person2 = personFile
    feature1 = infer(person1)[0]
    feature2 = infer(person2)[0]
    # 對角餘弦值
    dist = np.dot(feature1, feature2) / \
        (np.linalg.norm(feature1) * np.linalg.norm(feature2))
    if dist > 0.7:
        return True
    else:
        return False


def predictMulti(nums, folderName):
    speakers = [list()]
    speakers[0].append('test0.flac')

    for i in range(nums-1):
        # 要預測的兩個人的音頻文件
        person1 = 'segment/%s/' % folderName + 'test%s.flac' % i
        person2 = 'segment/%s/' % folderName + 'test%s.flac' % (i+1)
        feature1 = infer(person1)[0]
        feature2 = infer(person2)[0]
        # 對角餘弦值
        dist = np.dot(feature1, feature2) / \
            (np.linalg.norm(feature1) * np.linalg.norm(feature2))
        if dist > 0.7:
            for k in range(len(speakers)):
                if 'test%s.flac' % i in speakers[k]:
                    speakers[k].append("test%s.flac" % (i+1))
        else:
            for a in range(len(speakers)):
                num = (len(speakers))-1
                if (innerPredict(speakers[a][0], person2, folderName)):
                    speakers[a].append("test%s.flac" % (i+1))
                    break
                if (a == num and innerPredict(speakers[a][0], person2, folderName) == False):
                    speakers.append(list())
                    speakers[len(speakers)-1].append("test%s.flac" % (i+1))

    print(len(speakers))
    resultTxt = ""
    for j in range(nums):
        for i in range(len(speakers)):
            if 'test%s.flac' % j in speakers[i]:
                count = i + 1
                result = 'Speaker %d' % count + (' : {}'.format(
                    googleSpeech('segment/%s/' % folderName + 'test%s.flac' % j)))
                resultTxt += result
                resultTxt += '\n'

    print('Done')
    return resultTxt


def googleSpeech(fileName):
    file_name = fileName

    with io.open(file_name, 'rb') as audio_file:
        content = audio_file.read()
        audio = types.module.RecognitionAudio(content=content)

    config = types.module.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.FLAC,
        sample_rate_hertz=16000,
        language_code='en-US',
    )

    response = client.recognize(config, audio)

    for result in response.results:
        return(result.alternatives[0].transcript)
