from flask import Flask, request, Response
from werkzeug.utils import secure_filename
import json
import uuid
import os
import shutil
import io
import librosa
import numpy as np
import tensorflow as tf
import subprocess
from tensorflow.keras.models import Model
from pydub import AudioSegment
from pydub.silence import split_on_silence
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
from ffmpeg import audio_segment
from predictMulti import predictMulti
from predict import predict

def random_filename(fileName):
    ext = os.path.splitext(fileName)[1]
    new_filename = uuid.uuid4().hex + ext
    return new_filename

def delFold(path):
    shutil.rmtree(path, ignore_errors=True)
    os.mkdir(path)

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = './uploads'


@app.route('/', methods=['GET'])
def run():
    return "Flask backend"


@app.route('/speech', methods=['POST'])
def index():
    print(request)
    print(request.files)
    f = request.files['the_file']
    numbers = request.values.get('numbers')
    upload_filename = random_filename(f.filename)
    basepath = os.path.dirname(__file__)
    upload_path = os.path.join(
        basepath, 'uploads', upload_filename)
    f.save(upload_path)
    predictFile = audio_segment(upload_filename)
    if numbers == '2':
        result = predict(predictFile[0], predictFile[1])
    else:
        result = predictMulti(predictFile[0], predictFile[1])
    if f:
        delFold("converted")
        delFold("segment")
        delFold("uploads")
        return result
    else:
        return "No file uploaded"
    

app.run()
