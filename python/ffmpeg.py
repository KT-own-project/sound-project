# %%
import io
import os
import subprocess
from pydub import AudioSegment
from pydub.silence import split_on_silence


def convertToFlac(uploadFile):
    input_file = "uploads/%s" % uploadFile
    fname = os.path.splitext(uploadFile)[0]  # split the filename
    print(fname)
    os.mkdir('converted/'+fname)  # create a new folder use the uploadFile name
    output_file = "%s/out.flac" % fname
    subprocess.run(["ffmpeg", "-i", input_file, "-ar", "16000", "-ac", "1", "-acodec", "flac", "converted/"+output_file],
                   stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    return output_file, fname


def audio_segment(preSegmentFile):
    fileName = convertToFlac(preSegmentFile)
    root_path = fileName[1]
    os.mkdir('segment/'+root_path)
    sound = AudioSegment.from_file("converted/"+fileName[0])

    chunks = split_on_silence(
        sound, min_silence_len=550, silence_thresh=-45, keep_silence=1000)
    print("total: ", len(chunks))

    for i in list(range(len(chunks)))[::-1]:
        if len(chunks[i]) <= 1000:
            chunks.pop(i)
    print("useful: ", len(chunks))

    for j, chunk in enumerate(chunks):
        chunk.export("segment/%s/" % root_path +
                     "test{0}.flac".format(j), format="flac")

    return len(chunks), root_path
